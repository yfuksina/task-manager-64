package ru.tsc.fuksina.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.event.ConsoleEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileScanner {

    @NotNull
    private final File folder = new File("../");

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void init() {
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            try {
                publisher.publishEvent(new ConsoleEvent(fileName));
            } catch (Exception e) {
                bootstrap.getLoggerService().error(e);
            } finally {
                file.delete();
            }
        }
    }

    public void start() {
        init();
    }

}
