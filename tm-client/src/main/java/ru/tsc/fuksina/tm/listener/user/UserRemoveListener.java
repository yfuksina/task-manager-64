package ru.tsc.fuksina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.UserRemoveRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.event.ConsoleEvent;
import ru.tsc.fuksina.tm.util.TerminalUtil;

@Component
public final class UserRemoveListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-remove";

    @NotNull
    public static final String DESCRIPTION = "Remove user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    @EventListener(condition = "@userRemoveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserEndpoint().removeUser(new UserRemoveRequest(getToken(), login));
    }

}
