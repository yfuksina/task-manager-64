package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.component.ISaltProvider;
import ru.tsc.fuksina.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
