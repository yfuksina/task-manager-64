<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${viewName}</title>
    <style>
        td {
            padding: 5px;
            border: navy dashed 1px;
        }

        th {
            font-weight: 700;
            text-align: left;
            background: lightgrey;
            border: black solid 1px;
        }

        li {
            list-style-type: none;
            font-size: 20px;
            display: inline;
        }

        a{
            text-decoration: none;
            background-color: lightgrey;
            padding: 0 10px;
            border-radius: 4px;
            color: black;
        }
    </style>
</head>
<body>
<list>
    <li><a href="/tasks">Задачи</a></li>
    <li><a href="/projects">Проекты</a></li>
</list>
<hr>