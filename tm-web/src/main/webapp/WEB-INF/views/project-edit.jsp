<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Редактирование проекта</h1>

<form:form action="/project/edit/${project.id}/" method="POST" modelAttribute="project">

    <form:input type="hidden" path="id" />

    <p>
    <div>Название:</div>
    <div>
        <form:input type="text" path="name" />
    </div>
    </p>

    <p>
    <div>Описание:</div>
    <div>
        <form:input type="text" path="description" />
    </div>
    </p>

    <p>
    <div>Статус:</div>
    <div>
            <form:select path="status">
                <form:option value ="${null}" label="--- // ---" />
                <form:options items ="${statuses}" itemLabel="displayName" />
            </form:select>
        </div>
    </p>

    <p>
    <div>Дата начала:</div>
    <div>
        <form:input type="date" path="dateStart" />
    </div>
    </p>

    <p>
    <div>Дата окончания:</div>
    <div>
        <form:input type="date" path="dateEnd" />
    </div>
    </p>

    <button type="submit">Сохранить</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>