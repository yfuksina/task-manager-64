package ru.tsc.fuksina.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateEnd;

    @Nullable
    private String projectId = null;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
