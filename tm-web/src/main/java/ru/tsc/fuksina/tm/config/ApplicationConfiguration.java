package ru.tsc.fuksina.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.fuksina.tm")
public class ApplicationConfiguration {

}
