package ru.tsc.fuksina.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ProjectClearResponse extends AbstractResponse {
}
