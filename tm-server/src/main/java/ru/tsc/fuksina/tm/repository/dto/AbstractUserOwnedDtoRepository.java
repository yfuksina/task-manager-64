package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import ru.tsc.fuksina.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.List;

@NoRepositoryBean
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M> {

    @NotNull
    List<M> findAllByUserId(@NotNull final String userId);

    @NotNull
    @Query("SELECT m FROM ProjectDto m WHERE m.userId = :userId ORDER BY :sort")
    List<M> findAllByUserId(@NotNull @Param("userId") final String userId, @NotNull @Param("sort") String sort);

    @Nullable
    M findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

}
