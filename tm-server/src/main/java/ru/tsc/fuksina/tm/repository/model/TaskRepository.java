package ru.tsc.fuksina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
