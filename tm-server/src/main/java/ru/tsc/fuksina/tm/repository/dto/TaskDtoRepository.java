package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.dto.model.TaskDto;

import java.util.List;

@Repository
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
