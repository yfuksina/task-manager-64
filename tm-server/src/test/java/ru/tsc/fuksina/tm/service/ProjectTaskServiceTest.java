package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.fuksina.tm.api.service.dto.IProjectDtoService;
import ru.tsc.fuksina.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.fuksina.tm.api.service.dto.ITaskDtoService;
import ru.tsc.fuksina.tm.api.service.dto.IUserDtoService;
import ru.tsc.fuksina.tm.configuration.ServerConfiguration;
import ru.tsc.fuksina.tm.dto.model.UserDto;
import ru.tsc.fuksina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.fuksina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.fuksina.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.TaskIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;
import ru.tsc.fuksina.tm.marker.DatabaseCategory;

@Category(DatabaseCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    @NotNull
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private static String USER_ID;

    @NotNull
    private static String PROJECT_ID;

    @NotNull
    private static String TASK_ID;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        projectService = context.getBean(IProjectDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        projectTaskService = context.getBean(IProjectTaskDtoService.class);
        @NotNull final UserDto user = userService.create("user", "user");
        USER_ID = user.getId();
        PROJECT_ID = projectService.create(USER_ID, "project_1", "proj_1").getId();
        TASK_ID = taskService.create(USER_ID, "task_1", "task_1").getId();
    }

    @After
    public void end() {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID)
        );
        Assert.assertThrows(ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID)
        );
        Assert.assertThrows(TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "")
        );
        Assert.assertThrows(ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "123", TASK_ID)
        );
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "123")
        );
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        Assert.assertNotNull(taskService.findOneById(USER_ID, TASK_ID).getProjectId());
        Assert.assertEquals(PROJECT_ID, taskService.findOneById(USER_ID, TASK_ID).getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID)
        );
        Assert.assertThrows(ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID)
        );
        Assert.assertThrows(TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "")
        );
        Assert.assertThrows(ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "123", TASK_ID)
        );
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "123")
        );
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        Assert.assertNull(taskService.findOneById(USER_ID, TASK_ID).getProjectId());
    }

    @Test
    public void removeProjectById() {
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", PROJECT_ID)
        );
        Assert.assertThrows(ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(USER_ID, "")
        );
        projectTaskService.removeProjectById(USER_ID, PROJECT_ID);
        Assert.assertNull(projectService.findOneById(PROJECT_ID));
        Assert.assertNull(taskService.findOneById(TASK_ID));
    }

}
